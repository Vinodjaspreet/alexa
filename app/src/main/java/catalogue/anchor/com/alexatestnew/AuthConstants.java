/**
 * Copyright 2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License. A copy of the License is located the "LICENSE.txt"
 * file accompanying this source. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language governing permissions and limitations
 * under the License.
 */
package catalogue.anchor.com.alexatestnew;

public class AuthConstants {
    public static final String SESSION_ID = "sessionId";

    public static final String CLIENT_ID = "amzn1.application-oa2-client.c3bf73da8ceb43c6b8c149bdd7c5453f";
    public static final String REDIRECT_URI = "https://play.google.com/store";
    public static final String AUTH_CODE = "authCode";

    public static final String CODE_CHALLENGE = "ch1";
    public static final String CODE_CHALLENGE_METHOD = "m1";
    public static final String DSN = "dn1";
    public static final String PRODUCT_ID = "VSN";
}
